<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('abonnement_id')->nullable();
            $table->unsignedBigInteger('user_seance_id')->nullable();
            $table->unsignedBigInteger('commande_produit_id')->nullable();
            $table->unsignedBigInteger('mode_paiement_id');
            $table->double('montant');
            $table->string('statut');
            $table->dateTime('date');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('abonnement_id')->references('id')->on('abonnements')->onDelete('cascade');
            $table->foreign('user_seance_id')->references('id')->on('user_seance')->onDelete('cascade');
            $table->foreign('commande_produit_id')->references('id')->on('commande_produit')->onDelete('cascade');
            $table->foreign('mode_paiement_id')->references('id')->on('mode_paiements')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
