<?php

use App\Http\Controllers\AuthController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/dashboard', function () {
    return view('layouts.app');
})->middleware('auth')->name('home');

Route::get('/', [AuthController::class, 'login'])->middleware('guest')->name('login');
Route::post('/', [AuthController::class, 'doLogin']);

Route::post('/logout', [AuthController::class, 'logout'])->middleware('auth')->name('logout');