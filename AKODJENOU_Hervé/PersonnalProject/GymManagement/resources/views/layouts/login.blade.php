<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Connexion</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="/assets/img/icon.ico" type="image/x-icon" />
    <script src="/assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Lato:300,400,700,900"]
            },
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                    "simple-line-icons"
                ],
                urls: ['/assets/css/fonts.min.css']
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/atlantis.min.css">
    <link rel="stylesheet" href="/assets/css/demo.css">
</head>
<style>
    .full-screen-image {
        width: 100%;
        height: 100vh;
        object-fit: cover;
    }

    .login-container {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        text-align: center;
    }

    .form-label {
        display: flex;
        color: white !important;
        font-size: 17px !important
    }

    h1 {
        font-size: 30px
    }

    input {
        font-size: 15px !important
    }

    .remenber {
        font-size: 18px !important;
        color: white !important
    }

    .forgot {
        font-size: 18px !important;
    }

    button {
        font-size: 17px !important;
    }

    .signindiv {
        display: flex;
        justify-content: center
    }

    .signindiv p {
        font-size: 17px !important;
        color: white !important;
        margin-top: 6px
    }

    .signindiv a {
        font-size: 17px !important;
    }

    span {
        text-align: start !important;
        font-size: 15px;
    }

    .alert {
        font-size: 16px
    }
</style>

<body data-background-color="dark">
    <img src="/assets/img/peakpx1.jpg" alt="" class="full-screen-image">
    <div class="container login-container">
        <div class="col-md-6 col-12 offset-md-3">
            <div class="card card-stats card-round">
                <div class="card-body">
                    <div class="avatar avatar-xxl">
                        <img src="../../assets/img/jm_denis.jpg" alt="..." class="avatar-img rounded-circle">
                    </div>
                    <div class="row mt-4 d-flex justify-content-center">
                        <h1 class="text-white">Connexion</h1>
                    </div>
                    @if (session('error'))
                        <div class="alert mt-3 text-white alert-danger bg-danger" id="alertLogin">
                            {{ session('error') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col">
                            <form action="{{ route('login') }}" method="post">
                                @csrf
                                <div class="form-group @error('email') has-error has-feedback @enderror mt-3">
                                    <label for="email" class="form-label">Email </label>
                                    <input type="text" class="form-control" id="email" name="email"
                                        placeholder="ex:johnedoe@gmail.fr" value="{{ old('email') }}">
                                    @error('email')
                                        <span class="form-text text-muted text-danger">{{ $message }}</span>
                                    @enderror

                                </div>
                                <div class="form-group @error('password') has-error has-feedback @enderror  mt-3">
                                    <label for="password" class="form-label">Mot de passe</label>
                                    <input type="password" class="form-control" id="password" name="password">
                                    @error('password')
                                        <span class="form-text text-muted text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="remember"
                                                    id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <span for="remenber" class="form-check-sign remenber">Se souvenir de
                                                    moi</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <p>
                                            <a class="nav-link forgot" href="#">
                                                Mot de passe oublié ?
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary w-100 mt-4">Se connecter</button>
                            </form>
                            <div class="mt-4 signindiv">
                                <p>Vous n'avez de compte ?</p>
                                <a href="#" class="nav-link">S'incrire</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--   Core JS Files   -->
    <script src="/assets/js/core/jquery.3.2.1.min.js"></script>
    <script src="/assets/js/core/popper.min.js"></script>
    <script src="/assets/js/core/bootstrap.min.js"></script>

    <!-- jQuery UI -->
    <script src="/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script src="/assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

    <!-- jQuery Scrollbar -->
    <script src="/assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>


    <!-- Chart JS -->
    <script src="/assets/js/plugin/chart.js/chart.min.js"></script>

    <!-- jQuery Sparkline -->
    <script src="/assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

    <!-- Chart Circle -->
    <script src="/assets/js/plugin/chart-circle/circles.min.js"></script>

    <!-- Datatables -->
    <script src="/assets/js/plugin/datatables/datatables.min.js"></script>

    <!-- Bootstrap Notify -->
    <script src="/assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>

    <!-- jQuery Vector Maps -->
    <script src="/assets/js/plugin/jqvmap/jquery.vmap.min.js"></script>
    <script src="/assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"></script>

    <!-- Sweet Alert -->
    <script src="/assets/js/plugin/sweetalert/sweetalert.min.js"></script>

    <!-- Atlantis JS -->
    <script src="/assets/js/atlantis.min.js"></script>

    <!-- Atlantis DEMO methods, don't include it in your project! -->
    <script src="/assets/js/setting-demo.js"></script>
</body>
<script>
    function closeAlert(alertId) {
        var alertElement = document.getElementById(alertId);
        if (alertElement) {
            alertElement.style.display = 'none';
        }
    }
    setTimeout(function() {
        closeAlert('alertLogin');
    }, 3000);

</script>

</html>
