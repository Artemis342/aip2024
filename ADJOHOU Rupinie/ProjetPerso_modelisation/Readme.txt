**Projet de gestion de commande sur table pour restaurant**

Objectif: Ameliorer l'expérience client et l'éfficacité du service dans les restaurants en proposant une application intuitive et convivial permettant aux clients de passer commande directement depuis leur table.